class Response
  def awards(movies)
    response = ''
    movies.each do |movie|
      response = "#{response}#{single_movie(movie)}\n"
    end

    response
  end

  private

  def single_movie(movie)
    if movie.nil?
      'The movie does not exist'
    elsif !movie.won_awards?
      'Not available'
    else
      movie.awards
    end
  end
end
