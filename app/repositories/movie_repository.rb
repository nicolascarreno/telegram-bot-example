require_relative '../../model/movie'

class MovieRepository
  API_URL = 'https://www.omdbapi.com/'.freeze
  API_KEY = ENV['OMDB_API_KEY']

  def find_by_name(name)
    response = Faraday.get(API_URL, { t: name, apikey: API_KEY })
    response_json = JSON.parse(response.body)

    load_object(response_json) if movie_exists(response_json)
  end

  private

  def load_object(a_record)
    Movie.new(a_record['Title'], a_record['Awards'])
  end

  def movie_exists(response_json)
    response_json.key?('Title') && response_json.key?('Awards')
  end
end
