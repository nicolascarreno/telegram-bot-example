require_relative '../../bot_client_spec'

describe 'MovieRepository' do
  it 'should find by name' do
    stub_request_award_won
    movie = MovieRepository.new.find_by_name('Titanic')
    expect(movie.name).to eq 'Titanic'
    expect(movie.awards).to eq 'Won 11 Oscars, 126 wins & 83 nominations total'
  end

  it 'should return nil when movie does not exist' do
    stub_request_movie_doesnt_exist
    movie = MovieRepository.new.find_by_name('Moviedoesnotexist')
    expect(movie.nil?).to eq true
  end
end
