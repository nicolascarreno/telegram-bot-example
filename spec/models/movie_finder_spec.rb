describe MovieFinder do
  describe 'find_by_name' do
    it 'should return movie class when movie exists' do
      movie = Movie.new('Titanic', 'Won 11 Oscars, 126 wins & 83 nominations total')
      repo = instance_double('movie_repo', find_by_name: movie)
      movie_finder = described_class.new(repo)

      result = movie_finder.find_by_name('Titanic')
      expect(result.name).to eq 'Titanic'
      expect(result.awards).to eq 'Won 11 Oscars, 126 wins & 83 nominations total'
    end

    it 'should return nil when movie does not exist' do
      repo = instance_double('movie_repo', find_by_name: nil)
      movie_finder = described_class.new(repo)

      result = movie_finder.find_by_name('moviedoesnotexist')
      expect(result.nil?).to eq true
    end

    it 'should return nil when the wrong movie is returned' do
      movie = Movie.new('Dr. Strangelove', 'Nominated for 4 Oscars. 14 wins & 11 nominations total')
      repo = instance_double('movie_repo', find_by_name: movie)
      movie_finder = described_class.new(repo)

      result = movie_finder.find_by_name('Dr')
      expect(result.nil?).to eq true
    end
  end
end
