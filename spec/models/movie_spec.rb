require_relative '../../model/movie'

describe 'Movie' do
  it 'should have a name' do
    movie = Movie.new('Titanic', 'Won 3 Oscars. 14 wins & 11 nominations total')
    expect(movie.name).to eq 'Titanic'
  end

  it 'should have awards' do
    movie = Movie.new('Titanic', 'Won 3 Oscars. 14 wins & 11 nominations total')
    expect(movie.awards).to eq 'Won 3 Oscars. 14 wins & 11 nominations total'
  end
end
