class MovieFinder
  def initialize(movie_repo)
    @repo = movie_repo
  end

  def find_by_name(movie_name)
    movie = @repo.find_by_name(movie_name)

    if movie.nil? || movie.name != movie_name
      nil
    else
      movie
    end
  end
end
