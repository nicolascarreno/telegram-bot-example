class Movie
  def initialize(name, awards)
    @name = name
    @awards = awards
  end

  def won_awards?
    @awards != 'N/A'
  end

  attr_reader :name, :awards
end
