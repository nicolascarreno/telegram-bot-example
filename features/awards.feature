@wip
Feature: Awards
    Scenario: user uses command with an award winning movie
        When the user sends "/awards Titanic" to the bot
        Then the bot responds with "Won 11 Oscars, 126 wins & 83 nominations total"
    
    Scenario: user uses command with a movie with no awards and no nominations
        When the user sends "/awards Emoji" to the bot
        Then the bot responds with "Has no awards"

    Scenario: user uses command with a movie with only nominations
        When the user sends "/awards Heat" to the bot
        Then the bot responds with "15 nominations"

    Scenario: user uses command with a movie that does not exist
        When the user sends "/awards non existent movie" to the bot
        Then the bot responds with "The movie does not exist"

    Scenario: user uses command with multiple movie titles
        When the user sends "/awards Heat; Titanic; Emoji" to the bot
        Then the bot responds with "15 nominations"
        And the bot responds with "Won 11 Oscars, 126 wins & 83 nominations total"
        And the bot responds with "Has no awards or nominations"
